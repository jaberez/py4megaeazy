""" all functions to work with arduinoMEGAeasy project
"""
import socket
import urllib.request as request
from re import split, match, findall

DEFAULT_UDP_PORT = 8888

sock = socket.socket(socket.AF_INET,  # Internet
                     socket.SOCK_DGRAM)  # UDP


# global functions


def int_(str1):
    try:
        i = int(str1)
    except:
        i = str1
    return i


def get_url(url):
    return request.urlopen(url).read()


def send_msg(ip, cmd=""):
    """
    send message(command) to AME controllers
    :param ip: controller ip address
    :param port: port for UDP messaging
    :param cmd: command which needs to execute on controller
    :return:
    """
    if cmd == "":
        exit()
    sock.sendto(bytes(cmd, "utf-8"), (ip, DEFAULT_UDP_PORT))
    message, clientAddress = sock.recvfrom(2048)
    return [message, clientAddress]


def get_state(dict_, ame, ame_mod="", id=""):
    """
    get and parse result of state command from AME controller
    :param dict_:
    :param ame: controller id|ip address|name
    :param ame_mod: module on controller [cmd|but|rel|job|pir|and other ...]
    :param id: element id from selected mod  int|str 0 -all; str=",id=N"
    :return:
    """
    if type(id) == "int":
        id = ",id=" + str(id)
    if ame_mod in ("rel", "but", "cmd"):
        ame_mod = ":" + ame_mod
    else:
        ame_mod = ""
    for a in ame:
        print("Starting with:", a, flush=True)
        try:
            print("http://" + a + "/?cmd=state" + ame_mod + id + ";")
            page_source = str(get_url("http://" + a + "/?cmd=state" + ame_mod + id + ";"))
            print("connected", flush=True)
        except:
            print("Could not connect to controller:", a)
            page_source = ""

        if page_source != "":
            if ame_mod in ("rel", ""):
                relay_parse_state(dict_, page_source, a)
            elif ame_mod in ("", "but"):
                pass
    return dict_


def get_export(ame_ip, ame_mod, id):
    pass


# relay functions


def relay_turn_on(relays):
    """
    relay_turn_on(["2EN220"],["1EN220",3],["3HL_LD"],[2,1,"192.168.88.37"],[2,1,"AME_name"],[2,1,1])

    :param relays: list
        [0] str | int | list[str | int,...]
        if str: use as relay's name
        if int: use as id
        [1] int:  relay will be on for (timeOut*period)
        [2] str|int name|ip address|id of AME controller if type(relays[0])=="int"
    :return: list([ip, cmdstring], ...) ["192.168.88.37","ron:1,4;3;5,2;",...] or send cmd as message to AME controllers
            and return list of [[ame_ip,result],...]
    """
    pass


def relay_turn_off():
    pass


def relay_switch():
    pass


def relay_enable():
    pass


def relay_disable():
    pass


def relay_parse_state(dict_, source, ame_ip):
    """

    :param ame_ip: ip of AME controler
    :param dict_: dictionary(ies) of relays
    :param source: str with relays states to parse
    :return: dictionary(ies) of relays

    [(0)#, pn; name; state; Wts ; (5)WtCount; Avr.wph; %; mins; (9)timeout;]
    {relay_nameN: [{states ; WtCount; Avr.wph; %; mins; timeout;, ame_ip},...]
     in future       {ame_nameN: [[#, pn; name; state; Wts ; WtCount; Avr.wph; %; mins; timeout;],...],...}
     in future       {ame_idN: [[#, pn; name; state; Wts ; WtCount; Avr.wph; %; mins; timeout;],...],...}
    #
    # returns dict like
    # {"free": [{'id': 9, 'ip': '192.168.88.37', 'active': False, 'manual': False, 'enabled': True,
    # 'wtcount': 0, 'Avr.wph': 0, '%': '0(0)', 'mins': 0, 'timeout': 10},
    # {'id': 13, 'ip': '192.168.88.37', 'active': True, 'manual': False, 'enabled': True,
    # 'wtcount': 0, 'Avr.wph': 0, '%': '0(0)', 'mins': 5, 'timeout': 25}]}
    #
    # name; { states[1-3]; wtcount; Avr.wph;  %    ; mins ;timeout}
    """
    l = list()
    id = 1
    for line in findall("([\d]*)\)( |)([^\(]\d*)\[( {1,6}|)([\w]+|)](\[.+?])(.+?)\\\\n", source):
        line = [e for e in line]
        # line looks like
        # ('10', ' ', '37', '', '2LB_LD', "[<a href='?cmd=rsw:10;'>_</a>_<a href='?cmd=set:rel,10,enb,-1;'>E</a>]",
        # '     0;      0;      0;         0(0);     0;   15;')
        line[5] = findall(">(.)<", line[5])
        line[6] = list(map(lambda x: int_(x.strip()), split(";", line[6])))
        line = [line[4], [e for e in line[5]] + [e for e in line[6]][1:7]]
        # line
        # ['3bd_LD', ['_', '_', 'E', 0, 0, '0(0)', 0, 30, '']]
        if line[0] == "":  line[0] = "free"
        if dict_.get(line[0]) == None:
            dict_[line[0]] = list()
            dict_[line[0]].append(
                {"id": id, "ip": ame_ip, "active": line[1][0] == "A", "manual": line[1][1] == "M",
                 "enabled": line[1][2] == "E", "wtcount": line[1][3], "Avr.wph": line[1][4],
                 "%": line[1][5], "mins": line[1][6], "timeout": line[1][7]})
        else:
            dict_[line[0]].append(
                {"id": id, "ip": ame_ip, "active": line[1][0] == "A", "manual": line[1][1] == "M",
                 "enabled": line[1][2] == "E", "wtcount": line[1][3], "Avr.wph": line[1][4], "%": line[1][5],
                 "mins": line[1][6], "timeout": line[1][7]})
        id += 1
    return dict_


def relay_get_state():
    pass


def relay(cmd, r_name, ):
    eval("relay_" + cmd + "()")
